-- Float Window proportions
vim.g.floaterm_width = 0.85
vim.g.floaterm_height = 0.9

-- Vplit when open new window
vim.g.floaterm_opener = 'edit'
